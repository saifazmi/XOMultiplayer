// Usage:
//        java Server
//
// There is no provision for ending the server gracefully.  It will
// end if (and only if) something exceptional happens.


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * A class instantiating a server which accepts and hosts users who try to connect to it.
 * Also creates the sender-receiver threads necessary for communication of data from client-side to server-side.
 */

public class Server {

    public static void main(String[] args) {

        // A ClientTable is instantiated to hold users and their commands/messages.
        ClientTable clientTable = new ClientTable();

        //A GameTable is instantiated to hold users and their status dependent on whether they are involved in a game or not.
        GameTable gameTable = new GameTable();

        // Open a server socket:
        ServerSocket serverSocket = null;

        // We must try because it may fail with a checked exception:
        try {
            serverSocket = new ServerSocket(Integer.parseInt(args[0]));
            System.out.print("Server started.\n" +
                    "Waiting for incoming connections...\n");
        } catch (IOException e) {
            System.err.println("Couldn't listen on port " + Integer.parseInt(args[0]));
            System.exit(1); // Give up.
        }
        try {
            // We loop for ever, as servers usually do:

            while (true) {
                // Listen to the socket, accepting connections from new clients:
                Socket socket = serverSocket.accept();

                //Open a BufferedReader to read clientNames for addition to the ClientTable and passing to ServerReceiver.
                BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                // We ask the client what its name is:
                String clientName = fromClient.readLine();

                // For debugging:
                System.out.println(clientName + " joined the lobby");

                // We add the client to the table:
                clientTable.add(clientName);

                // We create and start a new thread to read from the client:
                (new ServerReceiver(clientName, fromClient, clientTable, gameTable)).start();

                // We create and start a new thread to write to the client:
                PrintStream toClient = new PrintStream(socket.getOutputStream());
                (new ServerSender(clientTable.getQueue(clientName), toClient)).start();
            }
        } catch (IOException e) {
            //if an Exception occurs, the server will tell whoever is viewing it.
            System.err.println("IO error " + e.getMessage());

        }
    }
}
