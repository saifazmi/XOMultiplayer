import java.io.BufferedReader;
import java.io.IOException;


/**
 * A class for getting commands & messages from the client and putting them in a queue for another thread to forward
 * to the appropriate client.
 */

public class ServerReceiver extends Thread {
    private String myClientsName;
    private BufferedReader myClient;
    private ClientTable clientTable;
    private GameTable gameTable;

    /**
     * Constructor for the ServerReceiver
     *
     * @param n  The name of a client inputting a particular command or message.
     * @param c  A BufferedReader for receiving input from the ClientSender and therefore the user.
     * @param t  The ClientTable of users and their command queues instantiated in the Server class.
     * @param gt The GameTable being shared among the program's threads, storing the names and status of players involved in a game.
     */
    public ServerReceiver(String n, BufferedReader c, ClientTable t, GameTable gt) {
        myClientsName = n;
        myClient = c;
        clientTable = t;
        gameTable = gt;
    }

    //We navigate between allowing the standard messaging service to run, and recognizing special user commands, using them to initiate suitable behaviours.
    public void run() {
        try {
            String command = null;


            while (true) {

                command = myClient.readLine();
                //if user inputs 'lobby', show a list of available clients (send command to queue)
                if (command.equals("lobby")) {
                    Message msg = new Message("Server", clientTable.getUsers());
                    MessageQueue commandsQueue = clientTable.getQueue(myClientsName);
                    if (commandsQueue != null)
                        commandsQueue.offer(msg);
                    else
                        System.err.println("Message for non-existent client " + command + ": " + "bla");
                    //if user inputs 'play', take a client name and initiate a game with that client (first sending a command to queue).
                } else if (command.equals("play")) {
                    String playWith = myClient.readLine();
                    System.out.println(playWith + " is playing? " + gameTable.isPlaying(playWith));
                    if (!gameTable.isPlaying(playWith)) {
                        gameTable.add(myClientsName, playWith);
                        String playRequest = "Would you like to play a game? (yes/no): ";
                        if (command != null && playWith != null) {
                            Message msg = new Message(myClientsName, playRequest);
                            MessageQueue commandsQueue = clientTable.getQueue(playWith);
                            if (commandsQueue != null)
                                commandsQueue.offer(msg);
                            else
                                System.err.println("Message for non-existent client " + command + ": " + playRequest);
                        }
                    } else {
                        if (command != null && playWith != null) {
                            String busy = "Sorry! Requested player busy.";
                            Message msg = new Message(myClientsName, busy);
                            MessageQueue commandsQueue = clientTable.getQueue(myClientsName);
                            if (commandsQueue != null)
                                commandsQueue.offer(msg);
                            else
                                System.err.println("Message for non-existent client " + command + ": " + busy);
                        }
                    }
                } else if (command.equals("yes") || command.equals("no")) {
                    String m = null;
                    if (command.equals("yes")) {
                        m = "Request accepted!";
                    } else {
                        m = "Request denied!";
                    }
                    Message msg = new Message(myClientsName, m);
                    MessageQueue commandsQueue = clientTable.getQueue(gameTable.playingWith(myClientsName));
                    if (commandsQueue != null)
                        commandsQueue.offer(msg);
                    else
                        System.err.println("Message for non-existent client " + command + ": " + m);
                    //if request is denied, remove client user from game table and enable others to request to play with them.
                    if (command.equals("no")) {
                        gameTable.remove(myClientsName, gameTable.playingWith(myClientsName));
                    }
                } else if (command.equals("quit")) {
                    clientTable.remove(myClientsName);
                    System.out.println(myClientsName + " left the lobby");
                } else {
                    myClient.close();
                    return;
                }

            }
        } catch (IOException e) {
            System.err.println("Something went wrong with the client " + myClientsName + " " + e.getMessage());
            // No point in trying to close sockets. Just give up.
            // We end this thread (we don't do System.exit(1)).
        } catch (NullPointerException e) {
            //potential error acknowledged but just a formality - no handling necessary in order to run.
        }
    }
}
