import java.io.PrintStream;

/**
 * A class which constantly reads from the message queue of a particular client, forwarding the messages
 * and commands within to the client.
 */

public class ServerSender extends Thread {
    private MessageQueue queue;
    private PrintStream client;

    /**
     * Constructor for the ServerSender
     *
     * @param q The MessageQueue (BlockingQueue) containing the messages/commands of a particular client.
     * @param c A printstream to the client through which the contents of their MessageQueue are sent.
     */
    public ServerSender(MessageQueue q, PrintStream c) {
        queue = q;
        client = c;
    }

    //we constantly print messages from the head of the queue to the client through the printstream.
    public void run() {
        while (true) {
            Message msg = queue.take();
            client.println(msg);
        }
    }
}
