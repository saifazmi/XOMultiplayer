import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A class for holding users playing together as Player1 and Player2.
 * Enables checking of PlayerStatus (playing or not playing) to determine whether further game
 * requests are allowed.
 */

public class GameTable {

    //instantiate a ConcurrentMap to hold the names of two players engaged in a game.
    private ConcurrentMap<String, String> gameTable = new ConcurrentHashMap<>();

    /**
     * A method for adding two players to the GameTable. If two users begin to play
     * together, they are added to the table and their status is set to playing,
     * which imposes certain restrictions.
     * @param player1 A first player involved in a game
     * @param player2 A second player involved in a game, and therefore "connected" to player1
     */
    public void add(String player1, String player2) {
        gameTable.put(player1, player2);
        gameTable.put(player2, player1);
    }

    /**
     * boolean method which checks whether a player is involved in a game on an
     * individual basis.
     * @param player A player possibly involved in a game.
     * @return A boolean true or false - is the player involved in a game or not?
     */

    public boolean isPlaying(String player) {
        return gameTable.containsKey(player);
    }

    /**
     * Method which determines who a player is playing with based on a client nickname passed in.
     * @param player The player whose opponent is to be determined.
     * @return The nickname of the opponent of the player passed in.
     */
    public String playingWith(String player) {
        return gameTable.get(player);
    }

    /**
     * Method for removing two players from the GameTable and hence signifying that their game together has ended.
     * @param player1 The first player to be removed.
     * @param player2 The second player to be removed.
     */

    public void remove(String player1, String player2) {
        gameTable.remove(player1);
        gameTable.remove(player2);
    }
}
