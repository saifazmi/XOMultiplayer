/**
 * A class for handling the messages/commands given by the client.
 *
 * @author mason
 */

public class Message {

    private final String sender;
    private final String text;

    /**
     * Constuctor for a message object, specifies the sender and the text itself to ensure a message gets to the
     * correct place with the correct content.
     *
     * @param sender The client who sent the message.
     * @param text
     */
    Message(String sender, String text) {
        this.sender = sender;
        this.text = text;
    }

    /**
     * Method for getting the sender of a particular message.
     *
     * @return The nickname of the client who sent the message (as a String).
     */
    public String getSender() {
        return sender;
    }

    /**
     * Method for getting the text of a particular message.
     *
     * @return A String consisting of the text sent by the client as a message.
     */

    public String getText() {
        return text;
    }

    /**
     * A method for displaying the name of a sender alongside the text of their message.
     * For the convenience of the client recipient - to see where a message received came from
     *
     * @return A String displaying the sender's nickname alongside their message for the recipient.
     */
    public String toString() {
        return "From " + sender + ": " + text;
    }
}
