import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;


/**
 * A class which repeatedly reads commands from the user, sending them to the server (read by ServerReceiver thread).
 */

public class ClientSender extends Thread {

    private String nickname;
    private PrintStream server;
    private boolean running; //condition with which to turn off/on the loop in case client exits.

    /**
     * Constructor for the ClientSender
     *
     * @param nickname The Nickname of the client/user inputting commands.
     * @param server   The PrintStream with which commands are sent to the ServerReceiver for processing.
     */
    ClientSender(String nickname, PrintStream server) {
        this.nickname = nickname;
        this.server = server;
        this.running = true;
    }

    public void run() {
        //BufferedReader instantiated to read user command inputs.
        BufferedReader user = new BufferedReader(new InputStreamReader(System.in));

        try {
            // Tell the server what my nickname is:
            server.println(nickname);
            String command = null;
            String playWith = null;
            // check for recognized commands and send them to the ServerReceiver via the printstream.
            //If no recognized commands are entered, maintain the functionality of the original messaging service regardless.
            while (running) {
                command = user.readLine();

                if (command.equals("lobby")) {
                    server.println(command);
                } else if (command.equals("play")) {
                    playWith = user.readLine();
                    server.println(command);
                    server.println(playWith);
                } else if (command.equals("quit")) {
                    this.running = false;
                    server.println(command);
                    System.exit(0);
                } else if (command.equals("yes") || command.equals("no")) {
                    server.println(command);
                } else {
                    System.out.println("Please input a valid command\n");
                }

            }
        } catch (IOException e) {
            System.err.println("Communication broke in ClientSender"
                    + e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Getter method to return the boolean of whether "running" variable is true or false.
     *
     * @return
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Setter method for changing the state of boolean "running" in order to interrupt the thread in the event the user exits.
     *
     * @param running
     */
    public void setRunning(boolean running) {
        this.running = running;
    }
}

