// We use https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/BlockingQueue.html

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A class for instantiating and handling the LinkedBlockingQueue which holds and communicates
 * client commands and messages.
 */

public class MessageQueue {

    //LinkedBlockingQueue instantiated, of the generic type Message (uses an object of type Message)
    private BlockingQueue<Message> queue = new LinkedBlockingQueue<Message>();

    /**
     * A method for the insertion of a message or command into the BlockingQueue.
     *
     * @param m The message to be placed in the BlockingQueue.
     */
    public void offer(Message m) {
        queue.offer(m);
    }

    /**
     * Retrieves and removes the head of this command/message queue, waiting if necessary until an element becomes available.
     *
     * @return The value at the of the queue - a particular command or message.
     */
    public Message take() {

        while (true) {
            try {
                return (queue.take());
            } catch (InterruptedException e) {
                //A necessary formality. No actual recovery code needed.
            }

        }
    }
}
