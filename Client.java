import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * A class for the client user to connect to the server through command line arguments user-nickname, portNumber, hostname
 */

class Client {

    public static void main(String[] args) {

        // Check correct amount of command line arguments and prompt if incorrect:
        if (args.length != 3) {
            System.err.println("Usage: java Client user-nickname portNumber hostname");
            System.exit(1); // Give up.
        }

        // Initialize information:
        String nickname = args[0];
        String port = args[1];
        String hostname = args[2];

        boolean isPlaying = false;

        // Open sockets, printstream & BufferedReader:
        PrintStream toServer = null;
        BufferedReader fromServer = null;
        Socket server = null;

        try {
            server = new Socket(hostname, Integer.parseInt(port));
            toServer = new PrintStream(server.getOutputStream());
            fromServer = new BufferedReader(new InputStreamReader(server.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Unknown host: " + hostname);
            System.exit(1); // Give up.
        } catch (IOException e) {
            System.err.println("The server doesn't seem to be running " + e.getMessage());
            System.exit(1); // Give up.
        }

        // Create two client threads:
        ClientSender sender = new ClientSender(nickname, toServer);
        ClientReceiver receiver = new ClientReceiver(fromServer);



        // Run them in parallel:
        sender.start();
        receiver.start();

        //provide welcome messages with instructions for the user.
        System.out.println(" ");
        System.out.println("Welcome to the client-server application.");
        System.out.println(" ");
        System.out.println("To see a list of other available users, input 'lobby'.");
        System.out.println(" ");
        System.out.println("To quit at any time, input 'quit'.");
        System.out.println(" ");
        System.out.println("Input 'play' and press enter, followed by another client's username to request to play a game with them.");
        System.out.println(" ");
        System.out.println("Otherwise, input a client name, pressing enter, followed by a message,");
        System.out.println("to send an instant message across the network.");
        System.out.println(" ");


        // Wait for client threads to end and close sockets.

        try {
            sender.join();
            toServer.close();
            receiver.join();
            fromServer.close();
            server.close();
        } catch (IOException e) {
            System.err.println("Something wrong " + e.getMessage());
            System.exit(1); // Give up.
        } catch (InterruptedException e) {
            System.err.println("Unexpected interruption " + e.getMessage());
            System.exit(1); // Give up.
        }
    }
}
