// Each nickname has a different incomming-message queue.

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A class for instantiating and handling the ConcurrentHashmap which holds Client user
 * names and Message/Command queues as key-value pairs.
 * Provides methods for adding, removing and getting the users, for returning userList,
 * and for returning the queue of a connected client.
 */

public class ClientTable {

    private ConcurrentMap<String, MessageQueue> queueTable = new ConcurrentHashMap<String, MessageQueue>();

    /**
     * Method for adding a client user to the ClientTable.
     * Adds client's nickname as specified at the command line and a new message
     * queue into the table as a key-value pair.
     *
     * @param nickname
     */
    public void add(String nickname) {
        queueTable.put(nickname, new MessageQueue());
    }

    /**
     * Method for removing a given client from the client table (ConcurrentHashMap). Provide the nickname and the key-value pair will be removed.
     *
     * @param nickname The nickname of the client desired for removal from the ClientTable.
     */

    public void remove(String nickname) {
        queueTable.remove(nickname);
    }

    /**
     * Method for getting the messagequeue/queue of commands of a particular client from the ClientTable.
     *
     * @param nickname The nickname of the client whose MessageQueue you want.
     * @return The MessageQueue of the specified client.
     */
    // Returns null if the nickname is not in the table:
    public MessageQueue getQueue(String nickname) {
        return queueTable.get(nickname);
    }

    /**
     * Method for returning a String of currently connected clients/users.
     * Used when the user calls 'lobby' to show a list of currently connected
     * potential opponents.
     *
     * @return A String consisting of the names of connected currently clients.
     */
    public String getUsers() {
        String users = "\n";
        for (String s : queueTable.keySet()) {
            users = users + s + "\n";
        }
        return users;
    }

}
