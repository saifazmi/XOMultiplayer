import java.io.BufferedReader;
import java.io.IOException;

/**
 * Class which gets messages from other clients via the server via ServerSender thread.
 */

public class ClientReceiver extends Thread {

    private BufferedReader server;
    private boolean running;

    /**
     * ClientReceiver constructor
     *
     * @param server a BufferedReader from which it reads data.
     */
    ClientReceiver(BufferedReader server) {
        this.server = server;
        this.running = true;
    }


    public void run() {
        // Print to the user whatever we get from the server:
        try {
            while (running) {
                String s = server.readLine();
                if (s != null) {
                    System.out.println(s);
                    if (s.contains("(yes/no)"))
                        System.out.print("> ");
                }
                else {
                    server.close(); // Probably no point.
                    throw new IOException("Got null from server"); // Caught below.
                }
            }
        } catch (IOException e) {
            System.out.println("Server seems to have died " + e.getMessage());
            System.exit(1); // Give up.
        }
    }
}
